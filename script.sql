
CREATE TABLE Client(
id VARCHAR(20),
nume VARCHAR(20),
adresa VARCHAR(40)
);

CREATE TABLE Produs(
id VARCHAR (20),
nume VARCHAR(20),
cantitate FLOAT,
pret FLOAT
);

CREATE TABLE Comanda(
id VARCHAR(20),
client VARCHAR (20),
produs VARCHAR(20),
cantitate FLOAT,
pret FLOAT
);


INSERT INTO Client(id,nume,adresa) VALUES(1,'Leonard','bld 5, Paris,Franta');
INSERT INTO Client(id,nume,adresa) VALUES(2,'Ionescu Paul','Observator,  Cluj Napoca, Romania');
INSERT INTO Client(id,nume,adresa) VALUES(3,'firma Dedeman','sediu social: sct 2, Bucuresti, Romania');
INSERT INTO Client(id,nume,adresa) VALUES(4,'Bill Gates','Seattle, Washington, SUA');
INSERT INTO Client(id,nume,adresa) VALUES(5,'Maria Dumitru','Alba Iulia, Romania');
INSERT INTO Client(id,nume,adresa) VALUES(6,'Lavinia Stoica','Cluj Napoca, Romania');
INSERT INTO Client(id,nume,adresa) VALUES(7,'Marina Popa','Brasov, Romania');

INSERT INTO Produs(id, nume, cantitate, pret) VALUES (1, "dulap alb", 1000, 700);
INSERT INTO Produs(id, nume, cantitate, pret) VALUES (2, "pat dormitor maro", 2000, 2500);
INSERT INTO Produs(id, nume, cantitate, pret) VALUES (3, "masuta de cafea", 1000, 400);
INSERT INTO Produs(id, nume, cantitate, pret) VALUES (4, "fotoliu piele", 400, 300);
INSERT INTO Produs(id, nume, cantitate, pret) VALUES (5, "scaun", 2000, 150);
INSERT INTO Produs(id, nume, cantitate, pret) VALUES (6, "birou", 1000, 800);
