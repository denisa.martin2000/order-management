package dao;

import model.Produs;

/**
 * Este o clasa care mosteneste clasa AbstractDao
 *
 * @author Denisa Martin
 * @since 18.4
 */
public class ProdusDao extends AbstractDao<Produs> {
    /**
     *  Constructor fara parametrii
     */
    public ProdusDao() {
    }
}
