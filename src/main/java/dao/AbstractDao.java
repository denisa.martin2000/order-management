package dao;

import connection.ConnectionFactory;

import java.lang.reflect.*;
import java.sql.*;
import java.util.List;
import java.util.logging.Logger;
import java.util.ArrayList;
import java.util.logging.Level;

/**
 * Implementeaza metodele de care avem nevoie ca sa lucram cu baza de date.
 *
 * @param <T> Java Model Class care este mapata in baza de date
 * @author Denisa Martin
 * @since 18.4
 */
public class AbstractDao<T> {
    protected static final Logger LOGGER = Logger.getLogger(AbstractDao.class.getName());
    private final Class<T> type;

    /**
     * Constructor
     */
    @SuppressWarnings("unchecked")
    public AbstractDao() {
        this.type = (Class<T>) ((ParameterizedType) getClass().getGenericSuperclass()).getActualTypeArguments()[0];
    }

    /**
     * Returneaza un string care descrie un query care selecteaza coloana field1, unde coloana field este agala cu un anumit parametru.
     *
     * @param field1 string care reprezinta numele coloanei
     * @param field  string care reprezinta valoarea
     * @return
     */
    private String createSelectQuery(String field1, String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT " + field1);
        sb.append(" FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + field + " =?");
        return sb.toString();
    }

    /**
     * Descrie un query, care selecteaza toate coloanele dintr-un tabel.
     *
     * @return string care descrie instructiunea
     */
    private String createSelectQuery2(String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT " + field + " FROM ");
        sb.append(type.getSimpleName());
        return sb.toString();
    }

    /**
     * Descrie un query, care selecteaza id-ul maxim.
     *
     * @return string care descrie instructiunea
     */
    private String createSelectMaxIdQuery() {
        StringBuilder sb = new StringBuilder();
        sb.append("SELECT MAX(id) FROM ");
        sb.append(type.getSimpleName());
        return sb.toString();
    }

    /**
     * Descrie un query, care insereaza valorile din t, intr-un anumit tabel.
     *
     * @param collumns numele coloanelor
     * @param t        Java Model Class care este mapata in baza de date
     * @return string care descrie instructiunea
     */
    private String createInsertQuery(String collumns, T t) {
        StringBuilder sb = new StringBuilder();
        sb.append("INSERT INTO ");
        sb.append(type.getSimpleName());
        sb.append("( " + collumns + ")");
        sb.append(" VALUES( " + t + ")");
        return sb.toString();
    }

    /**
     * Descrie un query, care sterge dintr-un tabel randurile in care coloana field are o anumita valoare.
     *
     * @param field numele coloanei
     * @return string care descrie instructiunea
     */
    private String createDeleteQuery(String field) {
        StringBuilder sb = new StringBuilder();
        sb.append("DELETE FROM ");
        sb.append(type.getSimpleName());
        sb.append(" WHERE " + field + "= ?;");
        return sb.toString();
    }

    /**
     * Descrie un query, care seteaza coloana field1 la valoarea field2 pentru randurile cu id-ul egal cu field3.
     *
     * @param field1 numele coloanei
     * @param field2 valoare
     * @param field3 valoare id
     * @return string care descrie instructiunea
     */
    private String createUpdateQuery(String field1, String field2, String field3) {
        StringBuilder sb = new StringBuilder();
        sb.append("UPDATE ");
        sb.append(type.getSimpleName());
        sb.append(" SET ");
        sb.append(field1 + "=" + field2);
        sb.append(" WHERE id=" + field3);
        return sb.toString();
    }

    /**
     * Executa query ul dat de createSelectQuery2() .
     *
     * @return resultSet cu toate datele din tabelul dorit
     */
    public ResultSet findAll() {
        Connection connection = null;
        String query = createSelectQuery2("*");
        ResultSet resultSet = null;
        try {
            connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            resultSet = statement.executeQuery();
            return resultSet;
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        }
        return null;
    }

    /**
     * Executa query-ul dat de createInsertQuery(collumns, t) si astfel insereaza o linie in tabel.
     *
     * @param collumns numele coloanelor
     * @param t        Java Model Class care este mapata in baza de date
     */
    public void insert(String collumns, T t) {
        Connection connection = null;
        String query = createInsertQuery(collumns, t);
        try {
            connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:insert " + e.getMessage());
        }
    }

    /**
     * Executa query ul dat de createDeleteQuery(field2) si astfel se sterg liniile din tabel care au coloana field1 egala cu valoarea field2.
     *
     * @param field1 numele coloanei
     * @param field2 valoarea
     */
    public void delete(String field1, String field2) {
        Connection connection = null;
        String query = createDeleteQuery(field1);
        try {
            connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(query, Statement.RETURN_GENERATED_KEYS);
            statement.setString(1, field2);
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        }
    }

    /**
     * Executa query-ul dat de createUpdateQuery(field1, field2, field3) si astfel seteaza coloana field1 la valoarea field2 daca id-ul este egal cu field3.
     *
     * @param field1 numele coloanei
     * @param field2 valoarea setata
     * @param field3 valoare id
     */
    public void edit(String field1, String field2, String field3) {
        Connection connection = null;
        String query = createUpdateQuery(field1, field2, field3);
        try {
            connection = ConnectionFactory.getConnection();
            PreparedStatement statement = connection.prepareStatement(query);
            statement.executeUpdate();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        }
    }

    /**
     * Alcatuieste un String[] cu numele field-urilor din obiectul specificat. Aceasta metoda este folosita pentru a crea header-ul unui tabel.
     *
     * @param object clasa dorita
     * @return numele coloanelor
     */
    public String[] getHeader(Object object) {
        ArrayList<String> columnNamesArrayList = new ArrayList<String>();
        for (Field field : object.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            columnNamesArrayList.add(field.getName());
        }
        String[] columnNames = new String[columnNamesArrayList.size()];
        columnNames = columnNamesArrayList.toArray(columnNames);
        return columnNames;
    }

    /**
     * Alcatuieste numele coloanelor unui tabel. Se foloseste la executarea instructiunii de insert.
     *
     * @param object clasa dorita
     * @return string care contine numele field-urilor dintr-un obiect
     */
    public String getCollumns(Object object) {
        StringBuilder sb = new StringBuilder();
        String prefix = "";
        for (Field field : object.getClass().getDeclaredFields()) {
            field.setAccessible(true);
            sb.append(prefix);
            prefix = " , ";
            sb.append(field.getName());
        }
        return sb.toString();
    }

    /**
     * Alcatuieste o lista cu valorile dintr-un ResultSet.
     *
     * @param rs resultSet
     * @return lista cu valorile din rs
     */
    public List<T> creatObject(ResultSet rs) {
        List<String> columnNames = new ArrayList<>();
        ResultSetMetaData rsmd = null;
        List<Object> rowData = new ArrayList<>();
        try {
            rsmd = rs.getMetaData();
            for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                columnNames.add(rsmd.getColumnLabel(i));
            }
            int rowIndex = 0;
            while (rs.next()) {
                rowIndex++;
                for (int i = 1; i <= rsmd.getColumnCount(); i++) {
                    rowData.add(rs.getObject(i));
                }
            }
        } catch (SQLException exception) {
            exception.printStackTrace();
        }
        return (List<T>) rowData;
    }

    /**
     * Alcatuieste o lista cu rezultatele returnate de executarea query-ului creayeSelectQuery3().
     *
     * @return lista rezultatele returnate
     */
    public List<T> getList() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery2("nume");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            return creatObject(resultSet);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        }
        return null;
    }

    /**
     * Se executa un query care returneaza intr-un ResultSet coloana field,
     * pentru randurile in care coloana nume este egala cu valoarea parametrului nume.
     *
     * @param field numele coloanei
     * @param nume  valoare
     * @return prima valoare din lista cu valorile date de resultSet
     */
    public T findByNume(String field, String nume) {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectQuery(field, "nume");
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            statement.setString(1, nume);
            resultSet = statement.executeQuery();
            return creatObject(resultSet).get(0);
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }

    /**
     * Se executa un query care returneaza intr-un ResultSet maximul dintr id-uri.
     *
     * @return String care reprezinta id-ul maxim
     */
    public String getMaxId() {
        Connection connection = null;
        PreparedStatement statement = null;
        ResultSet resultSet = null;
        String query = createSelectMaxIdQuery();
        try {
            connection = ConnectionFactory.getConnection();
            statement = connection.prepareStatement(query);
            resultSet = statement.executeQuery();
            return creatObject(resultSet).get(0).toString();
        } catch (SQLException e) {
            LOGGER.log(Level.WARNING, type.getName() + "DAO:findById " + e.getMessage());
        } finally {
            ConnectionFactory.close(resultSet);
            ConnectionFactory.close(statement);
            ConnectionFactory.close(connection);
        }
        return null;
    }
}
