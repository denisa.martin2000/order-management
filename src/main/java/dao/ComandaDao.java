package dao;

import model.Comanda;

/**
 * Este o clasa care mosteneste clasa AbstractDao
 *
 * @author Denisa Martin
 * @since 18.4
 */

public class ComandaDao extends AbstractDao<Comanda> {
}
