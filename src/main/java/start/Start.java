package start;

import presentation.Controller;
import presentation.Meniu;

import java.io.IOException;

/**
 * Porneste aplicatia.
 *
 * @author Denisa Martin
 * @since 13.4
 */
public class Start {
    /**
     * Metoda main
     *
     * @param args argumente
     */
    public static void main(String[] args) {
        Meniu frame = null;
        try {
            frame = new Meniu();
        } catch (IOException e) {
            e.printStackTrace();
        }
        Controller controller = new Controller(frame);
    }
}

