package bill;

import dao.ClientDao;
import model.Client;

/**
 * Implementeaza metodele de insert, delete si edit pentru clienti.
 *
 * @author Denisa Martin
 * @since 18.4
 */

public class ClientBill {
    private ClientDao clientDao;

    /**
     * Constructor
     */
    public ClientBill() {
        clientDao = new ClientDao();
    }

    /**
     * Apeleaza metoda de insert din clientDao si creeaza un nou client cu parametrii metodei.
     *
     * @param id      id ul clientului
     * @param name    numele clientului
     * @param address adresa clientului
     */
    public void insert(String id, String name, String address) {
        Client client = new Client(id, name, address);
        clientDao.insert(clientDao.getCollumns(client), client);
    }

    /**
     * Apeleaza metoda de delete din clientDao.
     * Sterge din tabel randurile in care valoarea din coloana field1 este egala cu valoarea field2.
     *
     * @param field1 numele coloanei
     * @param field2 valoarea
     */

    public void delete(String field1, String field2) {
        clientDao.delete(field1, field2);
    }

    /**
     * Apeleaza metoda edit din clientDao.
     * Seteaza valoarea din coloana field1 la valoarea field2, la randurile cu id-ul egal cu field3.
     *
     * @param field1 numele coloanei
     * @param field2 valoarea
     * @param field3 valoare id
     */

    public void edit(String field1, String field2, String field3) {
        clientDao.edit(field1, field2, field3);
    }
}
