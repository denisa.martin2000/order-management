package bill;

import dao.ComandaDao;
import model.Comanda;

/**
 * Implementeaza metoda de insert si delete pentru comenzi.
 *
 * @author Denisa Martin
 * @since 18.4
 */
public class ComandaBill {
    private ComandaDao comandaDao;

    /**
     * Constructor
     */
    public ComandaBill() {
        this.comandaDao = new ComandaDao();
    }

    /**
     * Apeleaza metoda de insert cu comandaDao si se creeaza o noua comanda cu parametrii corespunazatori.
     *
     * @param id        id comanda
     * @param client    nume client
     * @param produs    nume produs
     * @param cantitate cantitate produs
     * @param pret      pret comanda
     */
    public void insert(String id, String client, String produs, Float cantitate, Float pret) {
        Comanda comanda = new Comanda(id, client, produs, cantitate, pret);
        comandaDao.insert(comandaDao.getCollumns(comanda), comanda);
    }

    /**
     * Apeleaza metoda de delete din ProdusDao.
     *
     * @param field1 numele coloanei
     * @param field2 valoarea
     */
    public void delete(String field1, String field2) {
        comandaDao.delete(field1, field2);
    }

}
