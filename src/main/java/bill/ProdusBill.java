package bill;

import dao.ProdusDao;
import model.Produs;

/**
 * Implementeaza metodele de insert, delete si edit pentru produse.
 *
 * @author Denisa Martin
 * @since 18.4
 */
public class ProdusBill {
    private ProdusDao produsDao;

    /**
     * Constructor fara parametrii
     */
    public ProdusBill() {
        produsDao = new ProdusDao();
    }

    /**
     * Creeaza un nou produs cu parametrii metodei si apeleaza metoda de insert din ProdusDao cu numele coloanelor si noul produs.
     *
     * @param id        produs
     * @param name      nume produs
     * @param cantitate cantitate produs
     * @param pret      pret produs
     */
    public void insert(String id, String name, String cantitate, String pret) {
        Produs produs = new Produs(id, name, Float.parseFloat(cantitate), Float.parseFloat(pret));
        produsDao.insert(produsDao.getCollumns(produs), produs);
    }

    /**
     * Apeleaza metoda de delete din ProdusDao.
     *
     * @param field1 numele coloanei
     * @param field2 valoarea
     */

    public void delete(String field1, String field2) {
        produsDao.delete(field1, field2);
    }

    /**
     * Apeleaza metoda de edit din ProdusDao.
     *
     * @param field1 numele coloanei
     * @param field2 valoarea
     * @param field3 valoare id
     */
    public void edit(String field1, String field2, String field3) {
        produsDao.edit(field1, field2, field3);
    }


}
