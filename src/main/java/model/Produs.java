package model;

/**
 * Clasa care denumeste un produs.
 *
 * @author Denisa Martin
 * @since 12.4
 */

public class Produs {
    private String id;
    private String nume;
    private float cantitate;
    private float pret;

    /**
     * Constructor cu parametrii.
     *
     * @param id        id produs
     * @param nume      denumire produs
     * @param cantitate cantitate produs
     * @param pret      pret produs
     */
    public Produs(String id, String nume, float cantitate, float pret) {
        this.cantitate = cantitate;
        this.id = id;
        this.nume = nume;
        this.pret = pret;
    }

    /**
     * Constructor fara parametrii
     */
    public Produs() {

    }

    /**
     * Alcatuieste un string cu datele unui produs.
     *
     * @return string cu datele unui produs
     */

    public String toString() {
        return "'" + id + "'" + " , " + "'" + nume + "'" + " , " + cantitate + " , " + pret;
    }
}
