package model;

/**
 * Clasa care denumeste o comanda.
 *
 * @author Denisa Martin
 * @since 12.4
 */
public class Comanda {
    private String id;
    private String client;
    private String produs;
    private float cantitate;
    private float pret;

    /**
     * Constructor cu parametrii.
     *
     * @param id        comanda
     * @param client    nume client
     * @param produs    nume produs
     * @param cantitate cantitate comandata
     * @param pret      pret comanda
     */
    public Comanda(String id, String client, String produs, float cantitate, float pret) {
        this.cantitate = cantitate;
        this.client = client;
        this.id = id;
        this.produs = produs;
        this.pret = pret;
    }

    /**
     * Constructor fara parametrii
     */
    public Comanda() {

    }

    /**
     * Alcatuieste un string cu datele unei comenzi.
     *
     * @return string cu datele unei comenzi.
     */
    public String toString() {
        return "'" + id + "' , " + "'" + client + "' , " + "'" + produs + "' , " + cantitate + " , " + pret;
    }
}
