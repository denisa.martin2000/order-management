package model;

/**
 * Clasa care denumeste un client.
 *
 * @author Denisa Martin
 * @since 12.4
 */
public class Client {
    private String id;
    private String nume;
    private String adresa;

    /**
     * Constructor cu parametrii.
     *
     * @param id     id clinet
     * @param nume   nume client
     * @param adresa adresa client
     */
    public Client(String id, String nume, String adresa) {
        this.adresa = adresa;
        this.id = id;
        this.nume = nume;
    }

    /**
     * Constructor fara parametrii
     */
    public Client() {
    }

    /**
     * Alcatuieste un string cu datele unui client.
     *
     * @return string cu datele unui client
     */

    public String toString() {
        return "'" + id + "'" + ", " + "'" + nume + "'" + ", " + "'" + adresa + "'";
    }
}
