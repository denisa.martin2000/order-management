package presentation;

import bill.ClientBill;
import bill.ComandaBill;
import bill.ProdusBill;
import dao.ClientDao;
import dao.ComandaDao;
import dao.ProdusDao;
import model.Client;
import model.Comanda;
import model.Produs;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;

/**
 * Clasa care se ocupa cu actiunile utilizatorului cu interata.
 *
 * @author Denisa Martin
 * @since 19.4
 */

public class Controller {
    private Meniu meniu;

    /**
     * Constructor
     *
     * @param meniu meniu
     */
    public Controller(Meniu meniu) {
        this.meniu = meniu;

        meniu.getPanelClienti().addViewAllListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ClientDao p1 = new ClientDao();
                String header[] = p1.getHeader(new Client());
                DefaultTableModel aModel = new DefaultTableModel();
                aModel.setColumnIdentifiers(header);
                ResultSet rs = p1.findAll();
                ResultSetMetaData rsmd = null;
                try {
                    rsmd = rs.getMetaData();
                    int colNo = rsmd.getColumnCount();
                    while (rs.next()) {
                        Object[] objects = new Object[colNo];
                        for (int i = 0; i < colNo; i++) {
                            objects[i] = rs.getObject(i + 1);
                        }
                        aModel.addRow(objects);
                    }
                } catch (SQLException exception) {
                    exception.printStackTrace();
                }
                meniu.getPanelClienti().getTable().setModel(aModel);
            }
        });
        meniu.getPanelClienti().addAddListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String id;
                ClientBill clientBill = new ClientBill();
                if (meniu.getPanelClienti().getIdClient().equals("")) {
                    id = String.valueOf(Integer.parseInt(new ClientDao().getMaxId()) + 1);
                } else {
                    id = meniu.getPanelClienti().getIdClient();
                }
                clientBill.insert(id, meniu.getPanelClienti().getNumeClient(), meniu.getPanelClienti().getAdresa());
            }
        });
        meniu.getPanelClienti().addDeleteListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ClientBill clientBill = new ClientBill();
                if (!meniu.getPanelClienti().getIdClient().equals("")) {
                    clientBill.delete("id", meniu.getPanelClienti().getIdClient());
                } else if (!meniu.getPanelClienti().getNumeClient().equals("")) {
                    clientBill.delete("nume", meniu.getPanelClienti().getNumeClient());
                } else if (!meniu.getPanelClienti().getAdresa().equals("")) {
                    clientBill.delete("adresa", meniu.getPanelClienti().getAdresa());
                }
            }
        });
        meniu.getPanelClienti().addEditListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ClientBill clientBill = new ClientBill();
                if (!meniu.getPanelClienti().getNumeClient().equals("")) {
                    clientBill.edit("nume", "'" + meniu.getPanelClienti().getNumeClient() + "'", "'" + meniu.getPanelClienti().getIdClient() + "'");
                }
                if (!meniu.getPanelClienti().getAdresa().equals("")) {
                    clientBill.edit("adresa", "'" + meniu.getPanelClienti().getAdresa() + "'", "'" + meniu.getPanelClienti().getIdClient() + "'");
                }
            }
        });

        meniu.getPanelProduse().addViewAllListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProdusDao p1 = new ProdusDao();
                String header[] = p1.getHeader(new Produs());
                DefaultTableModel aModel = new DefaultTableModel();
                aModel.setColumnIdentifiers(header);
                ResultSet rs = p1.findAll();
                ResultSetMetaData rsmd = null;
                try {
                    rsmd = rs.getMetaData();
                    int colNo = rsmd.getColumnCount();
                    while (rs.next()) {
                        Object[] objects = new Object[colNo];
                        for (int i = 0; i < colNo; i++) {
                            objects[i] = rs.getObject(i + 1);
                        }
                        aModel.addRow(objects);
                    }
                } catch (SQLException exception) {
                    exception.printStackTrace();
                }
                meniu.getPanelProduse().getTable().setModel(aModel);
            }
        });

        meniu.getPanelProduse().addAddListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                String id;
                ProdusBill produsBill = new ProdusBill();
                if (meniu.getPanelProduse().getId().equals("")) {
                    id = String.valueOf(Integer.parseInt(new ProdusDao().getMaxId()) + 1);
                } else {
                    id = meniu.getPanelProduse().getId();
                }
                produsBill.insert(id, meniu.getPanelProduse().getNumeProdus(), meniu.getPanelProduse().getCantitate(), meniu.getPanelProduse().getPret());
            }
        });

        meniu.getPanelProduse().addDeleteListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProdusBill produsBill = new ProdusBill();
                if (!meniu.getPanelProduse().getId().equals("")) {
                    produsBill.delete("id", meniu.getPanelProduse().getId());
                } else if (!meniu.getPanelProduse().getNumeProdus().equals("")) {
                    produsBill.delete("nume", meniu.getPanelProduse().getNumeProdus());
                } else if (!meniu.getPanelProduse().getCantitate().equals("")) {
                    produsBill.delete("cantitate", meniu.getPanelProduse().getCantitate());
                } else if (!meniu.getPanelProduse().getPret().equals("")) {
                    produsBill.delete("pret", meniu.getPanelProduse().getPret());
                }
            }
        });
        meniu.getPanelProduse().addEditListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProdusBill produsBill = new ProdusBill();
                if (!meniu.getPanelProduse().getNumeProdus().equals("")) {
                    produsBill.edit("nume", "'" + meniu.getPanelProduse().getNumeProdus() + "'", "'" + meniu.getPanelProduse().getId() + "'");
                }
                if (!meniu.getPanelProduse().getCantitate().equals("")) {
                    produsBill.edit("cantitate", meniu.getPanelProduse().getCantitate(), "'" + meniu.getPanelProduse().getId() + "'");
                }
                if (!meniu.getPanelProduse().getPret().equals("")) {
                    produsBill.edit("pret", meniu.getPanelProduse().getPret(), "'" + meniu.getPanelProduse().getId() + "'");
                }
            }
        });

        meniu.getPanelComenzi().addViewAllListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ComandaDao p1 = new ComandaDao();
                String header[] = p1.getHeader(new Comanda());
                DefaultTableModel aModel = new DefaultTableModel();
                aModel.setColumnIdentifiers(header);
                ResultSet rs = p1.findAll();
                ResultSetMetaData rsmd = null;
                try {
                    rsmd = rs.getMetaData();
                    int colNo = rsmd.getColumnCount();
                    while (rs.next()) {
                        Object[] objects = new Object[colNo];
                        for (int i = 0; i < colNo; i++) {
                            objects[i] = rs.getObject(i + 1);
                        }
                        aModel.addRow(objects);
                    }
                } catch (SQLException exception) {
                    exception.printStackTrace();
                }
                meniu.getPanelComenzi().getTable().setModel(aModel);
            }
        });

        meniu.getPanelComenzi().addAddListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ProdusBill produsBill = new ProdusBill();
                ProdusDao produsDao = new ProdusDao();
                Float cantitateIntrodusa = Float.parseFloat(meniu.getPanelComenzi().getCantitateComanda());
                Float cantiate = Float.parseFloat(String.valueOf(produsDao.findByNume("cantitate", (String) meniu.getPanelComenzi().getList2().getSelectedValue())));
                Float cantitateNoua = cantiate - cantitateIntrodusa;
                if (cantitateIntrodusa < cantiate) {
                    produsBill.edit("cantitate", String.valueOf(cantitateNoua), String.valueOf(produsDao.findByNume("id", (String) meniu.getPanelComenzi().getList2().getSelectedValue())));
                    Float pret = Float.parseFloat(String.valueOf(produsDao.findByNume("pret", (String) meniu.getPanelComenzi().getList2().getSelectedValue())));
                    Float pretComanda = pret * cantitateIntrodusa;
                    ComandaBill comandaBill = new ComandaBill();
                    String id;
                    if (meniu.getPanelComenzi().getIdComanda().equals("")) {
                        id = String.valueOf(Integer.parseInt(new ComandaDao().getMaxId()) + 1);
                    } else {
                        id = meniu.getPanelComenzi().getIdComanda();
                    }
                    comandaBill.insert(id, (String) meniu.getPanelComenzi().getList1().getSelectedValue(), (String) meniu.getPanelComenzi().getList2().getSelectedValue(), cantitateIntrodusa, pretComanda);
                    Comanda comanda = new Comanda(id, (String) meniu.getPanelComenzi().getList1().getSelectedValue(), (String) meniu.getPanelComenzi().getList2().getSelectedValue(), cantitateIntrodusa, pretComanda);
                    try {
                        FileWriter myWriter = new FileWriter("factura.txt");
                        myWriter.write("Factura:" + comanda.toString());
                        myWriter.close();
                        System.out.println("Successfully wrote to the file.");
                    } catch (IOException exception) {
                        System.out.println("An error occurred.");
                        exception.printStackTrace();
                    }
                } else {
                    JOptionPane.showMessageDialog(meniu, "Cantitate gresita!");
                }
            }
        });
        meniu.getPanelComenzi().addDeleteListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                ComandaBill comandaBill = new ComandaBill();
                if (!meniu.getPanelComenzi().getIdComanda().equals("")) {
                    comandaBill.delete("id", meniu.getPanelComenzi().getIdComanda());
                }
            }
        });

    }

}
