package presentation;

import dao.ClientDao;

import dao.ProdusDao;

import javax.swing.*;
import java.awt.Color;
import java.awt.Font;

import java.awt.event.ActionListener;

/**
 * Alcatuieste panelul pentru comenzi.
 *
 * @author Denisa Martin
 * @since 18.4
 */

public class PanelComenzi extends JPanel {
    private JButton buttonViewAll;
    private JButton buttonAdd;
    private JButton buttonDelete;
    private JTable table;
    private JTextField id;
    private JTextField client;
    private JTextField cantitate;
    private JTextField produs;
    private JLabel idLabel;
    private JLabel nameLabel;
    private JLabel cantitateLabel;
    private JLabel pretLabel;
    private JList list1;
    private JList list2;

    /**
     * Constructor
     */
    public PanelComenzi() {
        Color color1 = new Color(239, 219, 210);
        Color color2 = new Color(209, 149, 120);
        setBackground(color1);
        setVisible(true);
        setBounds(0, 0, 470, 471);
        setLayout(null);

        //Titlu
        JLabel lblProduse = new JLabel("Comenzi");
        lblProduse.setForeground(color2);
        lblProduse.setFont(new Font("Tahoma", Font.BOLD, 25));
        lblProduse.setBounds(193, 33, 200, 31);
        add(lblProduse);

        //Button all
        buttonViewAll = new JButton("View all");
        buttonViewAll.setForeground(Color.WHITE);
        buttonViewAll.setBackground(color2);
        buttonViewAll.setBounds(183, 414, 100, 21);
        add(buttonViewAll);

        //Button add
        buttonAdd = new JButton("Add");
        buttonAdd.setForeground(Color.WHITE);
        buttonAdd.setBackground(color2);
        buttonAdd.setBounds(100, 180, 100, 21);
        add(buttonAdd);

        //Button Delete
        buttonDelete = new JButton("Delete");
        buttonDelete.setForeground(Color.WHITE);
        buttonDelete.setBackground(color2);
        buttonDelete.setBounds(250, 180, 100, 21);
        add(buttonDelete);

        id = new JTextField();
        id.setBounds(10, 130, 50, 21);
        id.setBackground(color1);
        add(id);

        ClientDao clientDao = new ClientDao();
        DefaultListModel DLM = new DefaultListModel();
        for (int i = 0; i < clientDao.getList().size(); i++) {
            DLM.addElement(clientDao.getList().get(i));
        }
        list1 = new JList(DLM);
        list1.setForeground(Color.WHITE);
        list1.setBackground(color2);
        list1.setBounds(80, 130, 100, 21);
        add(list1);
        JScrollPane jp2 = new JScrollPane(list1);
        jp2.setLocation(80, 130);
        jp2.setSize(110, 41);
        add(jp2);

        ProdusDao produsDao = new ProdusDao();
        DefaultListModel DLM2 = new DefaultListModel();
        for (int i = 0; i < produsDao.getList().size(); i++) {
            DLM2.addElement(produsDao.getList().get(i));
        }
        list2 = new JList(DLM2);
        list2.setForeground(Color.WHITE);
        list2.setBackground(color2);
        list2.setBounds(210, 130, 120, 21);
        add(list2);
        JScrollPane jp3 = new JScrollPane(list2);
        jp3.setLocation(210, 130);
        jp3.setSize(120, 41);
        add(jp3);

        cantitate = new JTextField();
        cantitate.setBounds(340, 130, 100, 21);
        cantitate.setBackground(color1);
        add(cantitate);

        idLabel = new JLabel("Id");
        idLabel.setBounds(10, 110, 120, 21);
        idLabel.setForeground(color2);
        add(idLabel);
        nameLabel = new JLabel("Client");
        nameLabel.setForeground(color2);
        nameLabel.setBounds(120, 110, 120, 21);
        add(nameLabel);
        cantitateLabel = new JLabel("Produs");
        cantitateLabel.setForeground(color2);
        cantitateLabel.setBounds(230, 110, 120, 21);
        add(cantitateLabel);
        pretLabel = new JLabel("Cantitate");
        pretLabel.setForeground(color2);
        pretLabel.setBounds(340, 110, 120, 21);
        add(pretLabel);

        table = new JTable();
        table.setForeground(Color.WHITE);
        table.setBackground(color2);
        table.setFillsViewportHeight(true);
        table.setBounds(10, 253, 450, 151);
        add(table);
        JScrollPane jp = new JScrollPane(table);
        jp.setLocation(10, 232);
        jp.setSize(450, 151);
        add(jp);

        JPanel panel = new JPanel();
        panel.setBackground(color2);
        panel.setBounds(10, 232, 450, 21);
        add(panel);
        JLabel fundal = new JLabel("");
        fundal.setIcon(new ImageIcon(getClass().getClassLoader().getResource("backkk.jpg")));
        fundal.setBounds(0, 0, 470, 471);
        add(fundal);
    }

    /**
     * Adauga un actionListener la buttonViewAll.
     *
     * @param action actiunea
     */

    public void addViewAllListener(ActionListener action) {
        this.buttonViewAll.addActionListener(action);
    }

    /**
     * Adauga un actionListener la buttonAdd.
     *
     * @param action actiunea
     */
    public void addAddListener(ActionListener action) {
        this.buttonAdd.addActionListener(action);
    }

    /**
     * Adauga un actionListener la buttonDelete.
     *
     * @param action actiunea
     */
    public void addDeleteListener(ActionListener action) {
        this.buttonDelete.addActionListener(action);
    }

    /**
     * Getter pentru tabelul table.
     *
     * @return JTable table
     */

    public JTable getTable() {
        return table;
    }

    /**
     * Getter pentru valoarea text din JTextField-ul id.
     *
     * @return valoarea din JTextField-ul id
     */
    public String getIdComanda() {
        return id.getText();
    }

    /**
     * Getter pentru valoarea text din JTextField-ul cantitate.
     *
     * @return valoarea din JTextField-ul cantitate
     */
    public String getCantitateComanda() {
        return cantitate.getText();
    }

    /**
     * Getter pentru list1
     *
     * @return list1
     */
    public JList getList1() {
        return list1;
    }

    /**
     * Getter pentru list2.
     *
     * @return list2
     */
    public JList getList2() {
        return list2;
    }
}