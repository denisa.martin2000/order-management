package presentation;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

/**
 * Alcatuieste panelul pentru clienti.
 *
 * @author Denisa Martin
 * @since 18.4
 */
public class PanelClient extends JPanel {
    private JButton buttonViewAll;
    private JButton buttonAdd;
    private JButton buttonDelete;
    private JButton buttonEdit;
    private JTable table;
    private JTextField id;
    private JTextField nume;
    private JTextField adresa;
    private JLabel idLabel;
    private JLabel nameLabel;
    private JLabel adressLabel;
    /**
     *  Constructor
     */
    public PanelClient() {
        Color color1 =new Color(239,219, 210);
        Color color2=new Color(209, 149, 120);
        setBackground(color1);
        setVisible(true);
        setBounds(0, 0, 470, 471);
        setLayout(null);

        //Titlu
        JLabel lblClienti = new JLabel("Clien\u021Bi");
        lblClienti.setForeground(color2);
        lblClienti.setFont(new Font("Tahoma", Font.BOLD, 25));
        lblClienti.setBounds(193, 33, 86, 31);
        add(lblClienti);

        //Button all
        buttonViewAll = new JButton("View all");
        buttonViewAll.setForeground(Color.WHITE);
        buttonViewAll.setBackground(color2);
        buttonViewAll.setBounds(183, 414, 100, 21);
        add(buttonViewAll);

        //Button add
        buttonAdd = new JButton("Add");
        buttonAdd.setForeground(Color.WHITE);
        buttonAdd.setBackground(color2);
        buttonAdd.setBounds(40, 180, 100, 21);
        add(buttonAdd);

        //Button delete
        buttonDelete = new JButton("Delete");
        buttonDelete.setForeground(Color.WHITE);
        buttonDelete.setBackground(color2);
        buttonDelete.setBounds(185, 180, 100, 21);
        add(buttonDelete);

        //Button edit
        buttonEdit = new JButton("Edit");
        buttonEdit.setForeground(Color.WHITE);
        buttonEdit.setBackground(color2);
        buttonEdit.setBounds(330, 180, 100, 21);
        add(buttonEdit);

        id = new JTextField();
        id.setBounds(30, 130, 120, 21);
        id.setBackground(color1);
        add(id);

        nume = new JTextField();
        nume.setBounds(175, 130, 120, 21);
        nume.setBackground(color1);
        add(nume);

        adresa = new JTextField();
        adresa.setBounds(320, 130, 120, 21);
        add(adresa);
        adresa.setBackground(color1);

        idLabel = new JLabel("Id");
        idLabel.setBounds(30, 110, 120, 21);
        idLabel.setForeground(color2);
        add(idLabel);
        nameLabel = new JLabel("Name");
        nameLabel.setBounds(175, 110, 120, 21);
        nameLabel.setForeground(color2);
        add(nameLabel);
        adressLabel = new JLabel("Address");
        adressLabel.setBounds(320, 110, 120, 21);
        adressLabel.setForeground(color2);
        add(adressLabel);
        table = new JTable();
        table.setForeground(Color.WHITE);
        table.setBackground(color2);
        table.setFillsViewportHeight(true);
        table.setBounds(10, 253, 450, 151);
        add(table);
        JScrollPane jp = new JScrollPane(table);
        jp.setLocation(10, 232);
        jp.setSize(450, 151);
        add(jp);

        JPanel panel = new JPanel();
        panel.setBackground(color2);
        panel.setBounds(10, 232, 450, 21);
        add(panel);
        JLabel fundal = new JLabel("");
        fundal.setIcon(new ImageIcon(getClass().getClassLoader().getResource("backkk.jpg")));
        fundal.setBounds(0, 0, 470, 471);
        add(fundal);
    }

    /**
     * Adauga un actionListener la buttonViewAll.
     *
     * @param action actiunea
     */
    public void addViewAllListener(ActionListener action) {
        this.buttonViewAll.addActionListener(action);
    }

    /**
     * Adauga un actionListener la buttonAdd.
     *
     * @param action actiunea
     */
    public void addAddListener(ActionListener action) {
        this.buttonAdd.addActionListener(action);
    }

    /**
     * Adauga un actionListener la buttonDelete.
     *
     * @param action actiunea
     */

    public void addDeleteListener(ActionListener action) {
        this.buttonDelete.addActionListener(action);
    }

    /**
     * Adauga un actionListener la buttonEdit.
     *
     * @param action actiunea
     */

    public void addEditListener(ActionListener action) {
        this.buttonEdit.addActionListener(action);
    }

    /**
     * Getter pentru table.
     *
     * @return JTable table.
     */

    public JTable getTable() {
        return table;
    }

    /**
     * Getter pentru valoarea text din JTextField-ul nume.
     *
     * @return valoarea din JTextField-ul nume
     */

    public String getNumeClient() {
        return nume.getText();
    }

    /**
     * Getter pentru valoarea text din JTextField-ul id.
     *
     * @return valoarea din JTextField-ul id
     */

    public String getIdClient() {
        return id.getText();
    }

    /**
     * Getter pentru valoarea text din JTextField-ul adresa.
     *
     * @return valoarea din JTextField-ul adresa
     */

    public String getAdresa() {
        return adresa.getText();
    }
}