package presentation;

import javax.swing.*;
import java.awt.Color;
import java.awt.Font;
import java.awt.event.ActionListener;

/**
 * Alcatuieste panelul pentru produse.
 *
 * @author Denisa Martin
 * @since 18.4
 */

public class PanelProduse extends JPanel {
    private JButton buttonViewAll;
    private JButton buttonAdd;
    private JButton buttonDelete;
    private JButton buttonEdit;
    private JTable table;
    private JTextField id;
    private JTextField nume;
    private JTextField cantitate;
    private JTextField pret;
    private JLabel idLabel;
    private JLabel nameLabel;
    private JLabel cantitateLabel;
    private JLabel pretLabel;
    /**
     *  Constructor
     */
    public PanelProduse() {
        Color color1 =new Color(239,219, 210);
        Color color2=new Color(209, 149, 120);
        setBackground(color1);
        setVisible(true);
        setBounds(0, 0, 470, 471);
        setLayout(null);

        //Titlu
        JLabel lblProduse = new JLabel("Produse");
        lblProduse.setForeground(color2);
        lblProduse.setFont(new Font("Tahoma", Font.BOLD, 25));
        lblProduse.setBounds(193, 33, 200, 31);
        add(lblProduse);

        //Button all
        buttonViewAll = new JButton("View all");
        buttonViewAll.setForeground(Color.WHITE);
        buttonViewAll.setBackground(color2);
        buttonViewAll.setBounds(183, 414, 100, 21);
        add(buttonViewAll);

        //Button add
        buttonAdd = new JButton("Add");
        buttonAdd.setForeground(Color.WHITE);
        buttonAdd.setBackground(color2);
        buttonAdd.setBounds(40, 180, 100, 21);
        add(buttonAdd);

        //Button delete
        buttonDelete = new JButton("Delete");
        buttonDelete.setForeground(Color.WHITE);
        buttonDelete.setBackground(color2);
        buttonDelete.setBounds(185, 180, 100, 21);
        add(buttonDelete);

        //Button edit
        buttonEdit = new JButton("Edit");
        buttonEdit.setForeground(Color.WHITE);
        buttonEdit.setBackground(color2);
        buttonEdit.setBounds(330, 180, 100, 21);
        add(buttonEdit);

        id = new JTextField();
        id.setBounds(10, 130, 100, 21);
        id.setBackground(color1);
        add(id);

        nume = new JTextField();
        nume.setBounds(120, 130, 100, 21);
        nume.setBackground(color1);
        add(nume);

        cantitate = new JTextField();
        cantitate.setBounds(230, 130, 100, 21);
        cantitate.setBackground(color1);
        add(cantitate);

        pret = new JTextField();
        pret.setBounds(340, 130, 100, 21);
        pret.setBackground(color1);
        add(pret);

        idLabel = new JLabel("Id");
        idLabel.setBounds(10, 110, 120, 21);
        idLabel.setForeground(color2);
        add(idLabel);
        nameLabel = new JLabel("Name");
        nameLabel.setBounds(120, 110, 120, 21);
        nameLabel.setForeground(color2);
        add(nameLabel);
        cantitateLabel = new JLabel("Quantity");
        cantitateLabel.setBounds(230, 110, 120, 21);
        cantitateLabel.setForeground(color2);
        add(cantitateLabel);
        pretLabel = new JLabel("Price");
        pretLabel.setBounds(340, 110, 120, 21);
        pretLabel.setForeground(color2);
        add(pretLabel);

        table = new JTable();
        table.setForeground(Color.WHITE);
        table.setBackground(color2);
        table.setFillsViewportHeight(true);
        table.setBounds(10, 253, 450, 151);
        add(table);
        JScrollPane jp = new JScrollPane(table);
        jp.setLocation(10, 232);
        jp.setSize(450, 151);
        add(jp);

        JPanel panel = new JPanel();
        panel.setBackground(color2);
        panel.setBounds(10, 232, 450, 21);
        add(panel);
        JLabel fundal = new JLabel("");
        fundal.setIcon(new ImageIcon(getClass().getClassLoader().getResource("backkk.jpg")));
        fundal.setBounds(0, 0, 470, 471);
        add(fundal);
    }

    /**
     * Adauga un actionListener la buttonViewAll.
     *
     * @param action actiunea
     */
    public void addViewAllListener(ActionListener action) {
        this.buttonViewAll.addActionListener(action);
    }

    /**
     * Adauga un actionListener la buttonAdd.
     *
     * @param action actiunea
     */
    public void addAddListener(ActionListener action) {
        this.buttonAdd.addActionListener(action);
    }

    /**
     * Adauga un actionListener la buttonDelete.
     *
     * @param action actiunea
     */
    public void addDeleteListener(ActionListener action) {
        this.buttonDelete.addActionListener(action);
    }

    /**
     * Adauga un actionListener la buttonEdit.
     *
     * @param action actiunea
     */
    public void addEditListener(ActionListener action) {
        this.buttonEdit.addActionListener(action);
    }

    /**
     * Getter pentru tabelul table.
     *
     * @return tabelul table
     */

    public JTable getTable() {
        return table;
    }

    /**
     * Getter pentru valoarea text din JTextField-ul nume.
     *
     * @return valoarea din JTextField-ul nume
     */
    public String getNumeProdus() { return this.nume.getText();}

    /**
     * Getter pentru valoarea text din JTextField-ul id.
     *
     * @return valoarea din JTextField-ul id
     */
    public String getId() {
        return id.getText();
    }

    /**
     * Getter pentru valoarea text din JTextField-ul cantitate.
     *
     * @return valoarea din JTextField-ul cantitate
     */
    public String getCantitate() {
        return cantitate.getText();
    }

    /**
     * Getter pentru valoarea text din JTextField-ul pret.
     *
     * @return valoarea din JTextField-ul pret
     */
    public String getPret() {
        return pret.getText();
    }
}