package presentation;


import javax.swing.JFrame;
import java.awt.Color;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import javax.swing.JPanel;
import javax.swing.JLabel;
import javax.swing.SwingConstants;
import javax.swing.ImageIcon;
import java.awt.Font;

/**
 * Se ocupa de meniul interfetei grafice.
 *
 * @author Denisa Martin
 * @since 18.4
 */

public class Meniu extends JFrame {
    private JFrame frame;
    private PanelClient panelClienti;
    private PanelProduse panelProduse;
    private PanelComenzi panelComenzi;


    /**
     * Constructor
     *
     * @throws IOException exceptie
     */
    public Meniu() throws IOException {
        initialize();
    }

    /**
     * Metoda de initializare
     */
    private void initialize() throws IOException {
        Color color =new Color(209, 149, 120);
        frame = new JFrame();
        frame.setTitle("Order Management");
        frame.getContentPane().setBackground(color);
        frame.getContentPane().setLayout(null);
        // se creaza panel pentru fiecare varianta din meniu

        panelClienti = new PanelClient();
        panelComenzi = new PanelComenzi();
        panelProduse = new PanelProduse();
        JPanel panel = new JPanel();
        panel.setBackground(color);
        panel.setBounds(0, 0, 286, 470);
        frame.getContentPane().add(panel);
        panel.setLayout(null);

        JLabel labelImagine = new JLabel("Order Management");
        labelImagine.setForeground(color);
        labelImagine.setFont(new Font("Verdana", Font.BOLD, 20));
        labelImagine.setBounds(41, 27, 300, 158);
        panel.add(labelImagine);
        //Produse
        JPanel paneProduse = new JPanel();
        paneProduse.setLayout(null);
        paneProduse.setBackground(color);
        paneProduse.setBounds(0, 203, 286, 45);
        panel.add(paneProduse);
        JLabel labelProduse = new JLabel("Produse");
        labelProduse.setIcon(new ImageIcon(getClass().getClassLoader().getResource("rsz_home.png")));
        labelProduse.setForeground(Color.WHITE);
        labelProduse.setFont(new Font("Tahoma", Font.PLAIN, 21));
        labelProduse.setBounds(44, 0, 123, 45);
        paneProduse.add(labelProduse);

        // Comenzi
        JPanel paneComenzi = new JPanel();
        paneComenzi.setBackground(color);
        paneComenzi.setBounds(0, 247, 286, 45);
        panel.add(paneComenzi);
        paneComenzi.setLayout(null);
        JLabel labelComenzi = new JLabel("Comenzi");
        labelComenzi.setIcon(new ImageIcon(getClass().getClassLoader().getResource("rsz_1cash.png")));
        labelComenzi.setForeground(Color.WHITE);
        labelComenzi.setFont(new Font("Tahoma", Font.PLAIN, 21));
        labelComenzi.setBounds(53, 0, 123, 45);
        paneComenzi.add(labelComenzi);

        //Clienti
        JPanel paneClienti = new JPanel();
        paneClienti.setBackground(color);
        paneClienti.setBounds(0, 292, 286, 45);
        panel.add(paneClienti);
        paneClienti.setLayout(null);
        JLabel labelClienti = new JLabel("Clien\u021Bi");
        labelClienti.setIcon(new ImageIcon(getClass().getClassLoader().getResource("rsz_male-icon-7928 (1).png")));
        labelClienti.setForeground(Color.WHITE);
        labelClienti.setFont(new Font("Tahoma", Font.PLAIN, 25));
        labelClienti.setBounds(52, 10, 136, 35);
        paneClienti.add(labelClienti);

        JLabel fundalMenu = new JLabel("");
        fundalMenu.setIcon(new ImageIcon(getClass().getClassLoader().getResource("fundalMeniuFinal.jpg")));
        fundalMenu.setBounds(0, 0, 286, 470);
        panel.add(fundalMenu);
        paneClienti.addMouseListener(new PanelButtonMouseAdapter(paneClienti) {
            @Override
            public void mouseClicked(MouseEvent e) {
                menuClicked(panelClienti);
            }
        });

        paneComenzi.addMouseListener(new PanelButtonMouseAdapter(paneComenzi) {
            @Override
            public void mouseClicked(MouseEvent e) {
                menuClicked(panelComenzi);
            }
        });

        paneProduse.addMouseListener(new PanelButtonMouseAdapter(paneProduse) {
            @Override
            public void mouseClicked(MouseEvent e) {
                menuClicked(panelProduse);
            }
        });
        //Main Content
        JPanel panelMainContent = new JPanel();
        panelMainContent.setBackground(color);
        panelMainContent.setBounds(287, 0, 471, 470);
        frame.getContentPane().add(panelMainContent);
        panelMainContent.setLayout(null);
        frame.setBounds(100, 100, 772, 507);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        panelMainContent.setLayout(null);
        panelMainContent.add(panelComenzi);
        panelMainContent.add(panelClienti);
        panelMainContent.add(panelProduse);
        menuClicked(panelClienti);
        frame.setVisible(true);
    }

    /**
     * Metoda pentru a face vizibil un anumit panel.
     *
     * @param panel panel ul dorit
     */
    public void menuClicked(JPanel panel) {
        panelClienti.setVisible(false);
        panelProduse.setVisible(false);
        panelComenzi.setVisible(false);
        panel.setVisible(true);

    }

    /**
     * Getter pentru panelClienti.
     *
     * @return panelClienti
     */
    public PanelClient getPanelClienti() {
        return panelClienti;
    }

    /**
     * Getter pentru panelComenzi.
     *
     * @return panelComenzi
     */
    public PanelComenzi getPanelComenzi() {
        return panelComenzi;
    }

    /**
     * Getter pentru panelProduse.
     *
     * @return panelProduse
     */
    public PanelProduse getPanelProduse() {
        return panelProduse;
    }

    /**
     * Selecteaza un anumit panel.
     */
    private class PanelButtonMouseAdapter extends MouseAdapter {

        JPanel panel;

        public PanelButtonMouseAdapter(JPanel panel) {
            this.panel = panel;
        }

        @Override
        public void mouseEntered(MouseEvent e) {
            panel.setBackground(new Color(200, 130, 110));
        }

        @Override
        public void mouseExited(MouseEvent e) {
            panel.setBackground(new Color(209, 149, 120));
        }

        @Override
        public void mousePressed(MouseEvent e) { panel.setBackground(new Color(190, 120, 100)); }

        @Override
        public void mouseReleased(MouseEvent e) { panel.setBackground(new Color(209, 149, 120)); }
    }
}
